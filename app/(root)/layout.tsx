import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../globals.css";
import Topbar from "@/components/shared/Topbar";
import LeftSidebar from "@/components/shared/LeftSidebar";
import Loading from "../(root)/loading";
import { Suspense } from "react";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "CONDO",
  description: "CONDO",
};

export default function RootLayout({ children }: { children: React.ReactNode; }) {
  return (
    
    <html lang='en'>
      <body className={inter.className}>
        <Topbar />

        <main className='flex flex-row'>
          <LeftSidebar />
          <Suspense fallback={<Loading/>}>
            <section className='main-container'>
              <div className='w-full'>{children}</div>
            </section>
          </Suspense>
        </main>
      </body>
    </html>
  );
}
