export default function Component() {
    return (
        <section className='mt-10 flex flex-col gap-5'>
            <div className="flex flex-wrap">
                <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                    <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg">
                        <div
                            className="flex-auto p-4"
                            style={{
                                border: "0px solid rgb(228, 228, 231)",
                                boxSizing: "border-box",
                                flex: "1 1 auto",
                                padding: "1rem",
                            }}
                        >
                            <div
                                className="flex flex-wrap"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    display: "flex",
                                    flexWrap: "wrap",
                                }}
                            >
                                <div
                                    className="relative w-full pr-4 max-w-full flex-grow flex-1"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "1 1 0%",
                                        flexGrow: 1,
                                        maxWidth: "100%",
                                        paddingRight: "1rem",
                                        position: "relative",
                                        width: "100%",
                                    }}
                                >
                                    <h5
                                        className="text-blueGray-400 uppercase font-bold text-xs"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            margin: "0px",
                                            fontWeight: 700,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            color: "rgba(148,163,184,1)",
                                            textTransform: "uppercase",
                                        }}
                                    >
                                        TRAFFIC
                                    </h5>
                                    <span
                                        className="font-semibold text-xl text-blueGray-700"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontWeight: 600,
                                            lineHeight: "1.75rem",
                                            fontSize: "1.25rem",
                                            color: "rgba(51,65,85,1)",
                                        }}
                                    >
                                        350,897
                                    </span>
                                </div>
                                <div
                                    className="relative w-auto pl-4 flex-initial"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "0 1 auto",
                                        paddingLeft: "1rem",
                                        position: "relative",
                                        width: "auto",
                                    }}
                                >
                                    <div
                                        className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-red-500"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(239,68,68,1)",
                                            borderRadius: "9999px",
                                            display: "inline-flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            height: "3rem",
                                            padding: "0.75rem",
                                            boxShadow:
                                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                                            textAlign: "center",
                                            color: "rgba(255,255,255,1)",
                                            width: "3rem",
                                        }}
                                    >
                                        <i
                                            className="far fa-chart-bar"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontWeight: 400,
                                                fontFamily: '"Font Awesome 5 Free"',
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <p
                                className="text-sm text-blueGray-400 mt-4"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    margin: "0px",
                                    fontSize: "0.875rem",
                                    lineHeight: "1.25rem",
                                    marginTop: "1rem",
                                    color: "rgba(148,163,184,1)",
                                }}
                            >
                                <span
                                    className="text-emerald-500 mr-2"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        marginRight: "0.5rem",
                                        color: "rgba(16,185,129,1)",
                                    }}
                                >
                                    <i
                                        className="fas fa-arrow-up"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontVariant: "normal",
                                            WebkitFontSmoothing: "antialiased",
                                            display: "inline-block",
                                            fontStyle: "normal",
                                            fontFeatureSettings: "normal",
                                            textRendering: "auto",
                                            lineHeight: 1,
                                            fontFamily: '"Font Awesome 5 Free"',
                                            fontWeight: 900,
                                        }}
                                    />{" "}
                                    3.48%
                                </span>
                                <span
                                    className="whitespace-nowrap"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    Since last month
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                    <div
                        className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg"
                        style={{
                            border: "0px solid rgb(228, 228, 231)",
                            boxSizing: "border-box",
                            backgroundColor: "rgba(255,255,255,1)",
                            borderRadius: "0.25rem",
                            display: "flex",
                            flexDirection: "column",
                            minWidth: "0px",
                            position: "relative",
                            boxShadow:
                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                            overflowWrap: "break-word",
                            marginBottom: "0px",
                        }}
                    >
                        <div
                            className="flex-auto p-4"
                            style={{
                                border: "0px solid rgb(228, 228, 231)",
                                boxSizing: "border-box",
                                flex: "1 1 auto",
                                padding: "1rem",
                            }}
                        >
                            <div
                                className="flex flex-wrap"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    display: "flex",
                                    flexWrap: "wrap",
                                }}
                            >
                                <div
                                    className="relative w-full pr-4 max-w-full flex-grow flex-1"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "1 1 0%",
                                        flexGrow: 1,
                                        maxWidth: "100%",
                                        paddingRight: "1rem",
                                        position: "relative",
                                        width: "100%",
                                    }}
                                >
                                    <h5
                                        className="text-blueGray-400 uppercase font-bold text-xs"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            margin: "0px",
                                            fontWeight: 700,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            color: "rgba(148,163,184,1)",
                                            textTransform: "uppercase",
                                        }}
                                    >
                                        NEW USERS
                                    </h5>
                                    <span
                                        className="font-semibold text-xl text-blueGray-700"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontWeight: 600,
                                            lineHeight: "1.75rem",
                                            fontSize: "1.25rem",
                                            color: "rgba(51,65,85,1)",
                                        }}
                                    >
                                        2,356
                                    </span>
                                </div>
                                <div
                                    className="relative w-auto pl-4 flex-initial"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "0 1 auto",
                                        paddingLeft: "1rem",
                                        position: "relative",
                                        width: "auto",
                                    }}
                                >
                                    <div
                                        className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-orange-500"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(249,115,22,1)",
                                            borderRadius: "9999px",
                                            display: "inline-flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            height: "3rem",
                                            padding: "0.75rem",
                                            boxShadow:
                                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                                            textAlign: "center",
                                            color: "rgba(255,255,255,1)",
                                            width: "3rem",
                                        }}
                                    >
                                        <i
                                            className="fas fa-chart-pie"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <p
                                className="text-sm text-blueGray-400 mt-4"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    margin: "0px",
                                    fontSize: "0.875rem",
                                    lineHeight: "1.25rem",
                                    marginTop: "1rem",
                                    color: "rgba(148,163,184,1)",
                                }}
                            >
                                <span
                                    className="text-red-500 mr-2"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        marginRight: "0.5rem",
                                        color: "rgba(239,68,68,1)",
                                    }}
                                >
                                    <i
                                        className="fas fa-arrow-down"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontVariant: "normal",
                                            WebkitFontSmoothing: "antialiased",
                                            display: "inline-block",
                                            fontStyle: "normal",
                                            fontFeatureSettings: "normal",
                                            textRendering: "auto",
                                            lineHeight: 1,
                                            fontFamily: '"Font Awesome 5 Free"',
                                            fontWeight: 900,
                                        }}
                                    />{" "}
                                    3.48%
                                </span>
                                <span
                                    className="whitespace-nowrap"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    Since last week
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                    <div
                        className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg"
                        style={{
                            border: "0px solid rgb(228, 228, 231)",
                            boxSizing: "border-box",
                            backgroundColor: "rgba(255,255,255,1)",
                            borderRadius: "0.25rem",
                            display: "flex",
                            flexDirection: "column",
                            minWidth: "0px",
                            position: "relative",
                            boxShadow:
                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                            overflowWrap: "break-word",
                            marginBottom: "0px",
                        }}
                    >
                        <div
                            className="flex-auto p-4"
                            style={{
                                border: "0px solid rgb(228, 228, 231)",
                                boxSizing: "border-box",
                                flex: "1 1 auto",
                                padding: "1rem",
                            }}
                        >
                            <div
                                className="flex flex-wrap"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    display: "flex",
                                    flexWrap: "wrap",
                                }}
                            >
                                <div
                                    className="relative w-full pr-4 max-w-full flex-grow flex-1"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "1 1 0%",
                                        flexGrow: 1,
                                        maxWidth: "100%",
                                        paddingRight: "1rem",
                                        position: "relative",
                                        width: "100%",
                                    }}
                                >
                                    <h5
                                        className="text-blueGray-400 uppercase font-bold text-xs"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            margin: "0px",
                                            fontWeight: 700,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            color: "rgba(148,163,184,1)",
                                            textTransform: "uppercase",
                                        }}
                                    >
                                        SALES
                                    </h5>
                                    <span
                                        className="font-semibold text-xl text-blueGray-700"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontWeight: 600,
                                            lineHeight: "1.75rem",
                                            fontSize: "1.25rem",
                                            color: "rgba(51,65,85,1)",
                                        }}
                                    >
                                        924
                                    </span>
                                </div>
                                <div
                                    className="relative w-auto pl-4 flex-initial"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "0 1 auto",
                                        paddingLeft: "1rem",
                                        position: "relative",
                                        width: "auto",
                                    }}
                                >
                                    <div
                                        className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-pink-500"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(236,72,153,1)",
                                            borderRadius: "9999px",
                                            display: "inline-flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            height: "3rem",
                                            padding: "0.75rem",
                                            boxShadow:
                                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                                            textAlign: "center",
                                            color: "rgba(255,255,255,1)",
                                            width: "3rem",
                                        }}
                                    >
                                        <i
                                            className="fas fa-users"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <p
                                className="text-sm text-blueGray-400 mt-4"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    margin: "0px",
                                    fontSize: "0.875rem",
                                    lineHeight: "1.25rem",
                                    marginTop: "1rem",
                                    color: "rgba(148,163,184,1)",
                                }}
                            >
                                <span
                                    className="text-orange-500 mr-2"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        marginRight: "0.5rem",
                                        color: "rgba(249,115,22,1)",
                                    }}
                                >
                                    <i
                                        className="fas fa-arrow-down"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontVariant: "normal",
                                            WebkitFontSmoothing: "antialiased",
                                            display: "inline-block",
                                            fontStyle: "normal",
                                            fontFeatureSettings: "normal",
                                            textRendering: "auto",
                                            lineHeight: 1,
                                            fontFamily: '"Font Awesome 5 Free"',
                                            fontWeight: 900,
                                        }}
                                    />{" "}
                                    1.10%
                                </span>
                                <span
                                    className="whitespace-nowrap"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    Since yesterday
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                    <div
                        className="relative flex flex-col min-w-0 break-words bg-white rounded mb-6 xl:mb-0 shadow-lg"
                        style={{
                            border: "0px solid rgb(228, 228, 231)",
                            boxSizing: "border-box",
                            backgroundColor: "rgba(255,255,255,1)",
                            borderRadius: "0.25rem",
                            display: "flex",
                            flexDirection: "column",
                            minWidth: "0px",
                            position: "relative",
                            boxShadow:
                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                            overflowWrap: "break-word",
                            marginBottom: "0px",
                        }}
                    >
                        <div
                            className="flex-auto p-4"
                            style={{
                                border: "0px solid rgb(228, 228, 231)",
                                boxSizing: "border-box",
                                flex: "1 1 auto",
                                padding: "1rem",
                            }}
                        >
                            <div
                                className="flex flex-wrap"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    display: "flex",
                                    flexWrap: "wrap",
                                }}
                            >
                                <div
                                    className="relative w-full pr-4 max-w-full flex-grow flex-1"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "1 1 0%",
                                        flexGrow: 1,
                                        maxWidth: "100%",
                                        paddingRight: "1rem",
                                        position: "relative",
                                        width: "100%",
                                    }}
                                >
                                    <h5
                                        className="text-blueGray-400 uppercase font-bold text-xs"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            margin: "0px",
                                            fontWeight: 700,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            color: "rgba(148,163,184,1)",
                                            textTransform: "uppercase",
                                        }}
                                    >
                                        PERFORMANCE
                                    </h5>
                                    <span
                                        className="font-semibold text-xl text-blueGray-700"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontWeight: 600,
                                            lineHeight: "1.75rem",
                                            fontSize: "1.25rem",
                                            color: "rgba(51,65,85,1)",
                                        }}
                                    >
                                        49,65%
                                    </span>
                                </div>
                                <div
                                    className="relative w-auto pl-4 flex-initial"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        flex: "0 1 auto",
                                        paddingLeft: "1rem",
                                        position: "relative",
                                        width: "auto",
                                    }}
                                >
                                    <div
                                        className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full bg-lightBlue-500"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(14,165,233,1)",
                                            borderRadius: "9999px",
                                            display: "inline-flex",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            height: "3rem",
                                            padding: "0.75rem",
                                            boxShadow:
                                                "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 10px 15px -3px rgba(0,0,0,0.1),0 4px 6px -2px rgba(0,0,0,0.05)",
                                            textAlign: "center",
                                            color: "rgba(255,255,255,1)",
                                            width: "3rem",
                                        }}
                                    >
                                        <i
                                            className="fas fa-percent"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                            <p
                                className="text-sm text-blueGray-400 mt-4"
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                    margin: "0px",
                                    fontSize: "0.875rem",
                                    lineHeight: "1.25rem",
                                    marginTop: "1rem",
                                    color: "rgba(148,163,184,1)",
                                }}
                            >
                                <span
                                    className="text-emerald-500 mr-2"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        marginRight: "0.5rem",
                                        color: "rgba(16,185,129,1)",
                                    }}
                                >
                                    <i
                                        className="fas fa-arrow-up"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            fontVariant: "normal",
                                            WebkitFontSmoothing: "antialiased",
                                            display: "inline-block",
                                            fontStyle: "normal",
                                            fontFeatureSettings: "normal",
                                            textRendering: "auto",
                                            lineHeight: 1,
                                            fontFamily: '"Font Awesome 5 Free"',
                                            fontWeight: 900,
                                        }}
                                    />{" "}
                                    12%
                                </span>
                                <span
                                    className="whitespace-nowrap"
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    Since last month
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section >
    );
}
