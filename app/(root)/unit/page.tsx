export default function Home() {
    return (
        <>
            <h1 className="head-text">Unit Details</h1>
            <div className="w-full mb-12 xl:mb-0 mt-5">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                    <div
                        className="block w-full overflow-x-auto"
                        style={{
                            border: "0px solid rgb(228, 228, 231)",
                            boxSizing: "border-box",
                            display: "block",
                            overflowX: "auto",
                            width: "100%",
                        }}
                    >
                        <table
                            className="items-center w-full bg-transparent border-collapse"
                            style={{
                                border: "0px solid rgb(228, 228, 231)",
                                boxSizing: "border-box",
                                borderColor: "inherit",
                                textIndent: "0px",
                                borderCollapse: "collapse",
                                alignItems: "center",
                                width: "100%",
                            }}
                        >
                            <thead>
                                <tr>
                                    <th
                                        className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"
                                        style={{
                                            backgroundColor: "rgba(248,250,252,1)",
                                            borderColor: "rgba(241,245,249,1)",
                                            borderStyle: "solid",
                                            borderWidth: "1px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontWeight: 600,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            paddingTop: "0.75rem",
                                            paddingBottom: "0.75rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            color: "rgba(100,116,139,1)",
                                            textTransform: "uppercase",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        Page name
                                    </th>
                                    <th
                                        className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(248,250,252,1)",
                                            borderColor: "rgba(241,245,249,1)",
                                            borderStyle: "solid",
                                            borderWidth: "1px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontWeight: 600,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            paddingTop: "0.75rem",
                                            paddingBottom: "0.75rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            color: "rgba(100,116,139,1)",
                                            textTransform: "uppercase",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        Visitors
                                    </th>
                                    <th
                                        className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(248,250,252,1)",
                                            borderColor: "rgba(241,245,249,1)",
                                            borderStyle: "solid",
                                            borderWidth: "1px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontWeight: 600,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            paddingTop: "0.75rem",
                                            paddingBottom: "0.75rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            color: "rgba(100,116,139,1)",
                                            textTransform: "uppercase",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        Unique users
                                    </th>
                                    <th
                                        className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            backgroundColor: "rgba(248,250,252,1)",
                                            borderColor: "rgba(241,245,249,1)",
                                            borderStyle: "solid",
                                            borderWidth: "1px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontWeight: 600,
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            paddingTop: "0.75rem",
                                            paddingBottom: "0.75rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            color: "rgba(100,116,139,1)",
                                            textTransform: "uppercase",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        Bounce rate
                                    </th>
                                </tr>
                            </thead>
                            <tbody
                                style={{
                                    border: "0px solid rgb(228, 228, 231)",
                                    boxSizing: "border-box",
                                }}
                            >
                                <tr
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <th
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        /argon/
                                    </th>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        4,569
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        340
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <i
                                            className="fas fa-arrow-up text-emerald-500 mr-4"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                                marginRight: "1rem",
                                                color: "rgba(16,185,129,1)",
                                            }}
                                        />
                                        46,53%
                                    </td>
                                </tr>
                                <tr
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <th
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        /argon/index.html
                                    </th>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        3,985
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        319
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <i
                                            className="fas fa-arrow-down text-orange-500 mr-4"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                                marginRight: "1rem",
                                                color: "rgba(249,115,22,1)",
                                            }}
                                        />
                                        46,53%
                                    </td>
                                </tr>
                                <tr
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <th
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        /argon/charts.html
                                    </th>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        3,513
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        294
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <i
                                            className="fas fa-arrow-down text-orange-500 mr-4"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                                marginRight: "1rem",
                                                color: "rgba(249,115,22,1)",
                                            }}
                                        />
                                        36,49%
                                    </td>
                                </tr>
                                <tr
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <th
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        /argon/tables.html
                                    </th>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        2,050
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        147
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <i
                                            className="fas fa-arrow-up text-emerald-500 mr-4"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                                marginRight: "1rem",
                                                color: "rgba(16,185,129,1)",
                                            }}
                                        />
                                        50,87%
                                    </td>
                                </tr>
                                <tr
                                    style={{
                                        border: "0px solid rgb(228, 228, 231)",
                                        boxSizing: "border-box",
                                    }}
                                >
                                    <th
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            textAlign: "left",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        /argon/profile.html
                                    </th>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        1,795
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        190
                                    </td>
                                    <td
                                        className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4"
                                        style={{
                                            border: "0px solid rgb(228, 228, 231)",
                                            boxSizing: "border-box",
                                            borderTopWidth: "0px",
                                            borderRightWidth: "0px",
                                            borderLeftWidth: "0px",
                                            fontSize: "0.75rem",
                                            lineHeight: "1rem",
                                            padding: "1rem",
                                            paddingLeft: "1.5rem",
                                            paddingRight: "1.5rem",
                                            verticalAlign: "middle",
                                            whiteSpace: "nowrap",
                                        }}
                                    >
                                        <i
                                            className="fas fa-arrow-down text-red-500 mr-4"
                                            style={{
                                                border: "0px solid rgb(228, 228, 231)",
                                                boxSizing: "border-box",
                                                fontVariant: "normal",
                                                WebkitFontSmoothing: "antialiased",
                                                display: "inline-block",
                                                fontStyle: "normal",
                                                fontFeatureSettings: "normal",
                                                textRendering: "auto",
                                                lineHeight: 1,
                                                fontFamily: '"Font Awesome 5 Free"',
                                                fontWeight: 900,
                                                marginRight: "1rem",
                                                color: "rgba(239,68,68,1)",
                                            }}
                                        />
                                        46,53%
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>
    );
}
