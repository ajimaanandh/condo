import React from "react";

export default function Component() {
  return (
    <>
    <section className="relative w-full h-full py-40 min-h-screen">
    <div className="absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full" ></div>
      <div
        className="flex-auto px-4 lg:px-10 py-10 pt-0"
        style={{
          border: "0px solid rgb(228, 228, 231)",
          boxSizing: "border-box",
          flex: "1 1 auto",
          paddingBottom: "2.5rem",
          paddingTop: "0px",
          paddingLeft: "2.5rem",
          paddingRight: "2.5rem",
        }}
      >
        <div
          className="text-blueGray-400 text-center mb-3 font-bold"
          style={{
            border: "0px solid rgb(228, 228, 231)",
            boxSizing: "border-box",
            fontWeight: 700,
            marginBottom: "0.75rem",
            textAlign: "center",
            color: "rgba(148,163,184,1)",
          }}
        >
          <small
            style={{
              border: "0px solid rgb(228, 228, 231)",
              boxSizing: "border-box",
              fontSize: "80%",
            }}
          >
            Or sign in with credentials
          </small>
        </div>
        <form
          style={{
            border: "0px solid rgb(228, 228, 231)",
            boxSizing: "border-box",
          }}
        >
          <div
            className="relative w-full mb-3"
            style={{
              border: "0px solid rgb(228, 228, 231)",
              boxSizing: "border-box",
              marginBottom: "0.75rem",
              position: "relative",
              width: "100%",
            }}
          >
            <label
              className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
              htmlFor="grid-password"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                display: "block",
                fontWeight: 700,
                fontSize: "0.75rem",
                lineHeight: "1rem",
                marginBottom: "0.5rem",
                color: "rgba(71,85,105,1)",
                textTransform: "uppercase",
              }}
            >
              Email
            </label>
            <input
              className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
              type="email"
              placeholder="Email"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                margin: "0px",
                fontFamily: "inherit",
                borderColor: "rgb(113, 113, 122)",
                padding: "0.5rem 0.75rem",
                appearance: "none",
                backgroundColor: "rgba(255,255,255,1)",
                borderRadius: "0.25rem",
                borderWidth: "0px",
                fontSize: "0.875rem",
                lineHeight: "1.25rem",
                paddingTop: "0.75rem",
                paddingBottom: "0.75rem",
                paddingLeft: "0.75rem",
                paddingRight: "0.75rem",
                boxShadow:
                  "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 1px 3px 0 rgba(0,0,0,0.1),0 1px 2px 0 rgba(0,0,0,0.06)",
                color: "rgba(71,85,105,1)",
                width: "100%",
                transitionProperty: "all",
                transitionTimingFunction: "linear",
                transitionDuration: "0.15s",
              }}
            />
          </div>
          <div
            className="relative w-full mb-3"
            style={{
              border: "0px solid rgb(228, 228, 231)",
              boxSizing: "border-box",
              marginBottom: "0.75rem",
              position: "relative",
              width: "100%",
            }}
          >
            <label
              className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
              htmlFor="grid-password"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                display: "block",
                fontWeight: 700,
                fontSize: "0.75rem",
                lineHeight: "1rem",
                marginBottom: "0.5rem",
                color: "rgba(71,85,105,1)",
                textTransform: "uppercase",
              }}
            >
              Password
            </label>
            <input
              className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
              type="password"
              placeholder="Password"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                margin: "0px",
                fontFamily: "inherit",
                borderColor: "rgb(113, 113, 122)",
                padding: "0.5rem 0.75rem",
                appearance: "none",
                backgroundColor: "rgba(255,255,255,1)",
                borderRadius: "0.25rem",
                borderWidth: "0px",
                fontSize: "0.875rem",
                lineHeight: "1.25rem",
                paddingTop: "0.75rem",
                paddingBottom: "0.75rem",
                paddingLeft: "0.75rem",
                paddingRight: "0.75rem",
                boxShadow:
                  "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 1px 3px 0 rgba(0,0,0,0.1),0 1px 2px 0 rgba(0,0,0,0.06)",
                color: "rgba(71,85,105,1)",
                width: "100%",
                transitionProperty: "all",
                transitionTimingFunction: "linear",
                transitionDuration: "0.15s",
              }}
            />
          </div>
          <div
            style={{
              border: "0px solid rgb(228, 228, 231)",
              boxSizing: "border-box",
            }}
          >
            <label
              className="inline-flex items-center cursor-pointer"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                cursor: "pointer",
                display: "inline-flex",
                alignItems: "center",
              }}
            >
              <input
                id="customCheckLogin"
                className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                type="checkbox"
                style={{
                  border: "0px solid rgb(228, 228, 231)",
                  boxSizing: "border-box",
                  margin: "0px",
                  fontFamily: "inherit",
                  fontSize: "100%",
                  lineHeight: "inherit",
                  padding: "0px",
                  borderColor: "rgb(113, 113, 122)",
                  appearance: "none",
                  WebkitPrintColorAdjust: "exact",
                  display: "inline-block",
                  verticalAlign: "middle",
                  backgroundOrigin: "border-box",
                  userSelect: "none",
                  flexShrink: 0,
                  backgroundColor: "rgb(255, 255, 255)",
                  borderRadius: "0.25rem",
                  borderWidth: "0px",
                  height: "1.25rem",
                  marginLeft: "0.25rem",
                  color: "rgba(51,65,85,1)",
                  width: "1.25rem",
                  transitionProperty: "all",
                  transitionTimingFunction: "linear",
                  transitionDuration: "0.15s",
                }}
              />
              <span
                className="ml-2 text-sm font-semibold text-blueGray-600"
                style={{
                  border: "0px solid rgb(228, 228, 231)",
                  boxSizing: "border-box",
                  fontWeight: 600,
                  fontSize: "0.875rem",
                  lineHeight: "1.25rem",
                  marginLeft: "0.5rem",
                  color: "rgba(71,85,105,1)",
                }}
              >
                Remember me
              </span>
            </label>
          </div>
          <div
            className="text-center mt-6"
            style={{
              border: "0px solid rgb(228, 228, 231)",
              boxSizing: "border-box",
              marginTop: "1.5rem",
              textAlign: "center",
            }}
          >
            <button
              className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
              type="button"
              style={{
                border: "0px solid rgb(228, 228, 231)",
                boxSizing: "border-box",
                margin: "0px",
                fontFamily: "inherit",
                backgroundImage: "none",
                cursor: "pointer",
                padding: "0px",
                appearance: "button",
                backgroundColor: "rgba(30,41,59,1)",
                borderRadius: "0.25rem",
                fontWeight: 700,
                fontSize: "0.875rem",
                lineHeight: "1.25rem",
                marginRight: "0.25rem",
                marginBottom: "0.25rem",
                outline: "transparent solid 2px",
                outlineOffset: "2px",
                paddingTop: "0.75rem",
                paddingBottom: "0.75rem",
                paddingLeft: "1.5rem",
                paddingRight: "1.5rem",
                boxShadow:
                  "var(--tw-ring-offset-shadow,0 0 transparent),var(--tw-ring-shadow,0 0 transparent),0 1px 3px 0 rgba(0,0,0,0.1),0 1px 2px 0 rgba(0,0,0,0.06)",
                color: "rgba(255,255,255,1)",
                textTransform: "uppercase",
                width: "100%",
                transitionProperty: "all",
                transitionTimingFunction: "linear",
                transitionDuration: "0.15s",
              }}
            >
              Sign In
            </button>
          </div>
        </form>
      </div>
      </section>
    </>
  );
}
