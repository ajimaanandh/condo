export const sidebarLinks = [
  {
    imgURL: "/assets/home.svg",
    route: "/dashboard",
    label: "Dashboad",
  },
  {
    imgURL: "/assets/unit.svg",
    route: "/unit",
    label: "Unit Details",
  },
  {
    imgURL: "/assets/billing.svg",
    route: "/billing",
    label: "E-Billing",
  },
  {
    imgURL: "/assets/support.svg",
    route: "/support",
    label: "E-Support",
  },
  {
    imgURL: "/assets/visitor.svg",
    route: "/visitor",
    label: "Visitor",
  },
  {
    imgURL: "/assets/community.svg",
    route: "/community",
    label: "Community",
  },
];
